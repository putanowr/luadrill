//! @brief Define MultibodyManager class for managaing application.

#include "config.h"
#include "hsMultibodyManager.h"
#include "hsSimEnviron.h"
#include "hsErrors.h"

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/stringbuffer.h>

#include "easylogging++.h"

#include <fstream>
#include <string>
#include <sstream>
#include <tclap/CmdLine.h>
#include <utility>
#include <windows.h>

namespace humsim
{

// Recipy from:
// https://stackoverflow.com/questions/40013355/how-to-merge-two-json-file-using-rapidjson
bool mergeObjects(rapidjson::Value &dstObject, rapidjson::Value &srcObject, rapidjson::Document::AllocatorType &allocator)
{
	for (auto srcIt = srcObject.MemberBegin(); srcIt != srcObject.MemberEnd(); ++srcIt)
	{
		auto dstIt = dstObject.FindMember(srcIt->name);
		if (dstIt == dstObject.MemberEnd())
		{
			rapidjson::Value dstName;
			dstName.CopyFrom(srcIt->name, allocator);
			rapidjson::Value dstVal;
			dstVal.CopyFrom(srcIt->value, allocator);

			dstObject.AddMember(dstName, dstVal, allocator);

			dstName.CopyFrom(srcIt->name, allocator);
			dstIt = dstObject.FindMember(dstName);
			if (dstIt == dstObject.MemberEnd())
				return false;
		}
		else
		{
			auto srcT = srcIt->value.GetType();
			auto dstT = dstIt->value.GetType();
			if (srcT != dstT)
				return false;

			if (srcIt->value.IsArray())
			{
				for (auto arrayIt = srcIt->value.Begin(); arrayIt != srcIt->value.End(); ++arrayIt)
				{
					rapidjson::Value dstVal;
					dstVal.CopyFrom(*arrayIt, allocator);
					dstIt->value.PushBack(dstVal, allocator);
				}
			}
			else if (srcIt->value.IsObject())
			{
				if (!mergeObjects(dstIt->value, srcIt->value, allocator))
					return false;
			}
			else
			{
				dstIt->value.CopyFrom(srcIt->value, allocator);
			}
		}
	}

	return true;
}

void MultibodyManager::SetLoggingDefaults() const
{
	el::Logger *verifyLogger = el::Loggers::getLogger("verify");
	el::Logger *contactLogger = el::Loggers::getLogger("contact");
	el::Configurations conf;
	conf.setGlobally(el::ConfigurationType::Format, "%msg");
  el::Loggers::reconfigureAllLoggers(conf);
}

void MultibodyManager::ConfigureLogger(const std::string &path) const
{
	el::Loggers::configureFromGlobal(path.c_str());
}

void MultibodyManager::set_request_(const std::string &arg) {
	for (size_t i = 0u; i < allowedRequests_.size(); ++i) {
		if (arg == std::string(allowedRequests_[i])) {
			requestType_ = arg;
			return;
		}
	}
	throw TCLAP::CmdLineParseException("Invalid request type", "request");
}

bool MultibodyManager::ParseCmdArgs(int argc, char *argv[]) {
	auto isOK = true;
	try {
		TCLAP::CmdLine cmd(PROJECT_NAME, ' ', PROJECT_VER);

		TCLAP::ValuesConstraint<std::string> allowedVals(allowedRequests_);

		TCLAP::ValueArg<std::string> requestArg("r", "request", "request type", false, "solve", &allowedVals, cmd);
		TCLAP::ValueArg<std::string> performanceFileArg("p", "performance-file", "performance log file", false, "", "string", cmd);
		TCLAP::ValueArg<std::string> bodyNameArg("n", "body-name", "body name", false, "Single", "string", cmd);
		TCLAP::ValueArg<std::string> inputFileArg("i", "input-file", "input file", true, "", "string", cmd);
		TCLAP::ValueArg<std::string> poseOutputFileArg("", "pose-file", "pose output file", false, "", "string", cmd);
		TCLAP::ValueArg<std::string> loggerConfigFileArg("", "logger-config", "logger config file", false, "", "string", cmd);
		TCLAP::ValueArg<double> maxTimeArg("t", "max-time", "maximal time", false, 2.0, "double", cmd);
		TCLAP::ValueArg<double> bodiesRestitutionScaleArg("", "bodies-restitution-scale", "scale for bodies restitution factor", false, -1.0, "double", cmd);

		TCLAP::ValueArg<double> poseXAngleArg("", "pose-x-angle", "x angle in degrees", false, 0.0, "double", cmd);
		TCLAP::ValueArg<double> poseYAngleArg("", "pose-y-angle", "y angle in degrees", false, 0.0, "double", cmd);
		TCLAP::ValueArg<double> poseZAngleArg("", "pose-z-angle", "z angle in degrees", false, 0.0, "double", cmd);

		TCLAP::ValueArg<int> numStepsArg("", "num-steps", "#steps to execute", false, -1, "int", cmd);
		TCLAP::ValueArg<int> numRefineArg("", "refine-level", "level of collision mesh refinement", false, 0, "int", cmd);
		TCLAP::ValueArg<int> maxElemCountArg("", "mesh-max-elem", "soft limit of mesh element count", false, 0, "int", cmd);
		TCLAP::ValueArg<int> voxelsPerBodyArg("", "voxels-per-body", "request voxelisation with specific density", false, -1, "int", cmd);
		TCLAP::ValueArg<int> voxelsPerVehicleArg("", "voxels-per-vehicle", "vehicle voxelisation density", false,
			humsim::hsconfig::default_vehicle_voxels_num, "int", cmd);
		TCLAP::SwitchArg strictVoxelLimitArg("", "strict-voxel-limit", "strictly limit number of voxels", cmd, false);

		TCLAP::SwitchArg doPoseViaKernelArg("", "pose-via-kernel", "perform positioning via multibody kernel", cmd, false);
		TCLAP::SwitchArg doContactOfJoinedBodiesArg("j", "joints-no-contact", "do not resolve contact of bodies directly connected", cmd, true);
		TCLAP::SwitchArg doBuildVehicleArg("", "build-vehicle", "build voxelised vehicle from moving boxes collection", cmd, false);
		TCLAP::SwitchArg doEchoArg("e", "echo", "print manager echo", cmd, false);
		TCLAP::SwitchArg doGMSHExportArg("g", "gmsh-export", "export steps files in GMSH format", cmd, false);
		TCLAP::SwitchArg doKeepTerminalArg("", "keep-terminal", "Keep terminal open till key is hit", cmd, false);
		TCLAP::SwitchArg doMeshCollisionArg("", "mesh-collision", "do collision with mesh", cmd, false);
		TCLAP::SwitchArg doPauseArg("", "pause-before", "pause before simulation", cmd, false);
		TCLAP::SwitchArg doSaveVoxelizedModelArg("", "save-voxelized-model", "save voxelized model", cmd, false);
		TCLAP::SwitchArg doShowSimulationArg("", "show-simulation", "visualise the simulation", cmd, false);
		TCLAP::SwitchArg doTestingArg("", "selftest", "run program in self-testing mode", cmd, false);
		TCLAP::SwitchArg doVTKExportArg("", "vtk-export", "export steps in VTK format", cmd, false);

		TCLAP::SwitchArg listPartsArg("", "list-bodies", "list model bodies", cmd, false);
		TCLAP::SwitchArg listBBoxArg("", "list-bbox", "list model bbox", cmd, false);
		TCLAP::SwitchArg listJointsArg("", "list-joints", "list model joints", cmd, false);
		TCLAP::SwitchArg listJointAnglesArg("", "list-angles", "list model joint angles", cmd, false);
		TCLAP::SwitchArg listBodiesVoxelsArg("", "list-bodies-voxels", "list bodies voxelisation", cmd, false);
		TCLAP::SwitchArg listVehicleVoxelsArg("", "list-vehicle-voxels", "list vehicle voxelisation", cmd, false);
		cmd.parse(argc, argv);

		bodyName = bodyNameArg.getValue();
		inputFile = inputFileArg.getValue();
		poseOutputFile = poseOutputFileArg.getValue();

		strictVoxelLimit = strictVoxelLimitArg.getValue();
		doTesting_ = doTestingArg.getValue();
		doWritePerformance_ = performanceFileArg.isSet();
		performanceFile_ = performanceFileArg.getValue();

		doBuildVehicle = doBuildVehicleArg.getValue();
		doContactOfJoinedBodies = doContactOfJoinedBodiesArg.getValue();
		doEcho = doEchoArg.getValue();
		doGMSHExport = doGMSHExportArg.getValue();
		doKeepTerminal = doKeepTerminalArg.getValue();
		doPause = doPauseArg.getValue();
		doMeshCollision = doMeshCollisionArg.getValue();
		doPoseViaKernel = doPoseViaKernelArg.getValue();
		doSaveVoxelizedModel = doSaveVoxelizedModelArg.getValue();
		doShowSimulation = doShowSimulationArg.getValue();
		doVTKExport = doVTKExportArg.getValue();

		doList_["bodies"] = listPartsArg.getValue();
		doList_["bbox"] = listBBoxArg.getValue();
		doList_["joints"] = listJointsArg.getValue();
		doList_["angles"] = listJointAnglesArg.getValue();
		doList_["bodies-voxels"] = listBodiesVoxelsArg.getValue();
		doList_["vehicle-voxels"] = listVehicleVoxelsArg.getValue();

		requestType_ = requestArg.getValue();

		bodiesRestitutionScale = bodiesRestitutionScaleArg.getValue();
		m_dMaxTime = maxTimeArg.getValue();
		numSteps = numStepsArg.getValue();
		numRefine = numRefineArg.getValue();
		maxElemCount = maxElemCountArg.getValue();
		voxelsPerBody = voxelsPerBodyArg.getValue();
		voxelsPerVehicle = voxelsPerVehicleArg.getValue();

		poseAngles.X = poseXAngleArg.getValue();
		poseAngles.Y = poseYAngleArg.getValue();
		poseAngles.Z = poseZAngleArg.getValue();

		SetLoggingDefaults();
	if (loggerConfigFileArg.isSet())
	{
	  ConfigureLogger(loggerConfigFileArg.getValue());
	}
	}
	catch (TCLAP::ArgException &e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		isOK = false;
		exitStatus = EXIT_FAILURE;
	}
	return isOK;
}

MultibodyManager::MultibodyManager() : allowedRequests_({"solve", "echo",
													  "list", "pose", "voxelise"})
{
	setup_props_();
}

void MultibodyManager::KeepTerminalAlive() const
{
	if (doKeepTerminal)
	{
		system("PAUSE");
	}
}
bool MultibodyManager::do_testing() const {
	return doTesting_;
}

void MultibodyManager::WritePerformanceInfo() const
{
	if (doWritePerformance_)
	{
		try
		{
			std::ofstream outfile;
			outfile.open(performanceFile_, std::ios_base::app);
			auto stamp = hsutils::GetTimestamp();
			outfile << std::string(stamp.begin(), stamp.end())
							<< " " << m_Timer.GetDuration("start_dynamics", "stop_dynamics")
							<< " " << m_Timer.GetTimeEnlapsedSince() << std::endl;
			outfile.close();
		}
		catch (const std::ios_base::failure &e)
		{
			std::cerr << "Cannot write performance info: " << e.what() << std::endl;
		}
	}
}

bool MultibodyManager::do_list(const std::string &key) const {
	auto iter = doList_.find(key);
	if (iter != doList_.end()) {
		return iter->second;
	}
	else {
		std::stringstream oss;
		oss << "Invalid listing request key: " << key;
		throw humsim::ManagerError(oss.str());
	}
}

void MultibodyManager::setup_props_() {
	props_.SetObject();
	rapidjson::Value dummy_int(123);
	rapidjson::Value dummy_double(1.5);
	props_.AddMember("dummy_int", dummy_int, props_.GetAllocator());
	props_.AddMember("dummy_double", dummy_double, props_.GetAllocator());
	auto sKey = std::string("dummy_string");
	auto sVal = std::string("dummy_value");
	rapidjson::Value key(sKey.c_str(), props_.GetAllocator());
	rapidjson::Value value(sVal.c_str(), props_.GetAllocator());
	props_.AddMember(key, value, props_.GetAllocator());
}


// This checks for keys not for paths to children.
void MultibodyManager::check_prop_exists_(const std::string &key) const {
	if (!props_.HasMember(key.c_str())) {
		std::stringstream oss;
		oss << "ERROR: Cannot find property : " << key;
		throw std::logic_error(oss.str());
	}
}

std::string MultibodyManager::request_type() const {
	return requestType_;
}

bool MultibodyManager::write(const std::string &path, bool overwrite) const {
	std::ofstream ofs(path);
	rapidjson::OStreamWrapper osw(ofs);
	rapidjson::Writer<rapidjson::OStreamWrapper> writer(osw);
	props_.Accept(writer);
	ofs.close();
	return true;
}

void MultibodyManager::echo(std::ostream &out) const {
	out << "MultibodyManager settings: " << std::endl;
	rapidjson::OStreamWrapper osw(out);
	rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(osw);
	props_.Accept(writer);
	out << "--------------------------" << std::endl;
}

void MultibodyManager::verify_() const {
	bool state = true;
	if (state != true) {
		throw std::logic_error("Configuration file is invalid: missing parameters");
	}
}

bool MultibodyManager::read(const std::string &path) {
	std::ifstream ifs(path);
	rapidjson::IStreamWrapper isw(ifs);
	rapidjson::Document tmp;
	tmp.ParseStream(isw);
	mergeObjects(props_, tmp, props_.GetAllocator());
	verify_();
	return true;
}

} // namespace humsim


