//! @brief Define Manager class for managing luadrill programm 

#define RAPIDJSON_HAS_STDSTRING 1

#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <map>

namespace luadrill {


class LuaDrillManager
{
public:
	LuaDrillManager();
	bool ParseCmdArgs(int argc, char *argv[]);

	void SetLoggingDefaults() const;
	void ConfigureLogger(const std::string &path) const;
	void WritePerformanceInfo() const;

  	//! Pause program execution till key is hit
  	void KeepTerminalAlive() const;
  	bool read(const std::string &path);
  	bool write(const std::string &path, bool overwrite) const;
  	void echo(std::ostream &out = std::cout) const;

  	bool do_list(const std::string &key) const;

  	//! @brief Return true if program should be run in selftesting mode.
  	bool do_testing() const;

  	std::string request_type() const;

  	//! Name of the input file. It can be empty.
  	std::string inputFile;

  	std::string poseOutputFile;
  	CVector3Dd poseAngles;

  	bool noLog = false;
  	bool writeInfo = false;

	//! @brief If true simulator tries to resolve contact of bodies connected by a joint
	bool doContactOfJoinedBodies = false;
	bool doEcho = false;
	//! @brief If true then build voxelised vehicle from moving boxes collection
	bool doKeepTerminal = false;


  	int exitStatus = EXIT_SUCCESS;

  	hsutils::Timer m_Timer;

private:
	void check_prop_exists_(const std::string &key) const;
	void set_request_(const std::string &arg);
	void setup_props_();
	void verify_() const;

	rapidjson::Document props_;
	std::vector<std::string> allowedRequests_;
 	std::string requestType_;
	std::map<std::string, bool> doList_;
  	std::string performanceFile_;
  	bool doTesting_ = false;
  	bool doWritePerformance_ = false;
};

} // namespace luadrill 
