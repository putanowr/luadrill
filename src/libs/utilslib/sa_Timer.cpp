//! @brief Implementation of Timer class

#include "sa_Timer.h"

#include <algorithm>
#include <sstream>
#include <utility>

namespace sa_utils
{

std::string GetTimestamp()
{
  std::time_t time_now = std::time(nullptr);
  struct tm buf;
  char str[26];
  localtime_s(&buf, &time_now);
  asctime_s(str, sizeof str, &buf);
  str[strlen(str) - 1] = '\0';
  return std::string(str);
}

const std::string Timer::unit = "ms";

Timer::Timer()
{
  MarkMoment("_start");
}

void Timer::MarkMoment(const char *name)
{
  auto t = std::chrono::high_resolution_clock::now();
  std::string s(name);
  m_Moments.emplace_back(std::make_pair(s, t));
}

Timer::Moment Timer::FindPrevious(const char *name) const
{
  const auto itr = std::find_if(m_Moments.begin(), m_Moments.end(), special_compare(std::string(name)));
  if (itr != m_Moments.end())
  {
    if (itr != m_Moments.begin())
      return *(std::prev(itr));
    else
      return *itr;
  }
  else
  {
    std::stringstream oss;
    oss << "Cannot find Moment called : " << name;
    throw std::runtime_error(oss.str());
  }
}

Timer::Moment Timer::Find(const char *name) const
{
  const auto itr = std::find_if(m_Moments.begin(), m_Moments.end(), special_compare(std::string(name)));
  if (itr != m_Moments.end())
  {
    return *itr;
  }
  else
  {
    std::stringstream oss;
    oss << "Cannot find Moment called : " << name;
    throw std::runtime_error(oss.str());
  }
}

double Timer::GetDurationAsDouble(TimePoint start, TimePoint end)
{
  const std::chrono::duration<double, std::milli> duration = end - start;
  return duration.count();
}

double Timer::GetTotalTime() const
{
  const auto start = m_Moments.front().second;
  const auto end = m_Moments.back().second;
  return GetDurationAsDouble(start, end);
}

double Timer::GetDuration(const char* startMoment, const char *stopMoment) const
{
  const auto start = Find(startMoment).second;
  const auto end = Find(stopMoment).second;
  return GetDurationAsDouble(start, end);
}

double Timer::GetDuration(const char* moment) const
{
  const auto start = FindPrevious(moment).second;
  const auto end = Find(moment).second;
  return GetDurationAsDouble(start, end);
}

double Timer::GetTimeEnlapsedSince(const char *moment) const
{
  TimePoint start;
  if (moment != nullptr)
    start = Find(moment).second;
  else
    start = m_Moments.front().second;
  const auto end = std::chrono::high_resolution_clock::now();
  return GetDurationAsDouble(start, end);
}
} // namespace sa_utils
