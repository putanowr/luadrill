//! @brief Define utility class for managing enlapsed time.

#pragma once

#include <chrono>
#include <list>
#include <string>
#include <iomanip>
#include <time.h>

namespace sa_utils
{

//! Write time stamp
  std::string GetTimestamp();

//! Class for easier management of time duration
//! This class can store named time moment and get duration between them

class Timer 
{
public:
  Timer();
  //! Save time at he call moment under given name
  void MarkMoment(const char *name);
  //! Return time in milliseconds between timer creation and the last recorded moment.
  double GetTotalTime() const;
  //! Get time duration in milliseconds between two named moments.
  double GetDuration(const char *startMoment, const char *stopMoment) const;
  //! Get time duration in milliseconds between given moment and a previous saved one.
  double GetDuration(const char *moment) const;
  //! Get time enlapsed since named moment
  double GetTimeEnlapsedSince(const char *moment=nullptr) const;

  static const std::string unit;

protected:

  using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;
  using Moment = std::pair<std::string, TimePoint>;

  static double GetDurationAsDouble(TimePoint start, TimePoint end);

  Moment Find(const char *name) const;
  Moment FindPrevious(const char *name) const;

  std::list<Moment> m_Moments;

  struct special_compare : public std::unary_function<Moment, bool>
  {
    explicit special_compare(const std::string &refmoment) : baseline(refmoment) {}
    bool operator() (const Moment &arg) const
    {
      return arg.first == baseline;
    }
    std::string baseline;
  };
};

} // namespace sa_utils
