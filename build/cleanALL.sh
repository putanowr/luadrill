#!/bin/bash

# SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
# SPDX-License-Identifier: MIT

rm -fr bin docs src tests external \
		ALL_BUILD.vcxproj ALL_BUILD.vcxproj.filters \
		CMakeCache.txt \
		CMakeDoxyfile.in \
		CMakeDoxygenDefaults.cmake \
		CMakeFiles \
		cmake_install.cmake \
		CTestTestfile.cmake \
		Debug \
		INSTALL.vcxproj \
		INSTALL.vcxproj.filters \
		luadrill.sln \
		Makefile \
		myeasylog.log \
		Release \
		Testing \
		Win32 \
		ZERO_CHECK.vcxproj \
		ZERO_CHECK.vcxproj.filters
if [ $? ]; then 
	echo "Cleaned the build directory ... enjoy"
else
	echo "ERROR while cleaning the build directory"
	exit 22
fi
